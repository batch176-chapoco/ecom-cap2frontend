const productsData = [
    {
        id: "lpr-001",
        name: "Dog Leash",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.Voluptas esse iusto ad modi quos cum, at alias nostrum nobis neque aliquid porro magnam tenetur ea facilis quo aut explicabo? Ex!",
        price: 45000
    },
    {
        id: "lpr-002",
        name: "Dog Leash (RED)",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.Voluptas esse iusto ad modi quos cum, at alias nostrum nobis neque aliquid porro magnam tenetur ea facilis quo aut explicabo? Ex!",
        price: 50000
    },
    {
        id: "lpr-003",
        name: "Dog Leash (BLUE)",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit.Voluptas esse iusto ad modi quos cum, at alias nostrum nobis neque aliquid porro magnam tenetur ea facilis quo aut explicabo? Ex!",
        price: 55000
    },
]
export default productsData;