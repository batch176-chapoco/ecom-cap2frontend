import './App.css';
import AppNavBar from './components/AppNavbar';
import Home from './pages/Home';
import Login from './pages/Login';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { useState } from 'react';
import { UserProvider } from './UserContext';
import Products from './pages/Products';
import Register from './pages/Register';
import Logout from './pages/Logout';
import SpecificProduct from './pages/SpecificProduct';
import CartView from './pages/CartView';
import Orders from './pages/Orders';



function App() {

  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  const unsetUser = () => {
    localStorage.clear();
  }
  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <BrowserRouter>

        <AppNavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/products" element={<Products />} />
          <Route path="/register" element={<Register />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/products/:productId" element={<
            SpecificProduct />} />
          <Route path="/cartview" element={<CartView />} />
          <Route path="/orders" element={<Orders />} />
        </Routes>

      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
