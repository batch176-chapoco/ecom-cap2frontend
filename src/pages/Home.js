import Banner from "../components/Banner"
import BootstrapCarousel from "../components/BootstrapCarousel"
import CarouselText from "../components/CarouselText"




export default function Home() {
    return (
        <>
            <Banner />
            <CarouselText />
            <BootstrapCarousel />



        </>
    )
}