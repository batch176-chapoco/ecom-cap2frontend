import { useEffect, useContext, useState } from "react";
import UserView from "../components/UserView";
import UserContext from '../UserContext';
import AdminView from '../components/AdminView';




export default function Products() {

    const [allProducts, setAllProducts] = useState([]
    )

    const fetchData = () => {
        fetch('https://capstone2trial.herokuapp.com/products/activeProducts')
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setAllProducts(data)
            })
    }

    useEffect(() => {
        fetchData()
    }, [])

    const { user } = useContext(UserContext);

    return (
        <>
            <h1>PRODUCTS</h1>

            {(user.isAdmin === true) ?

                <AdminView productsData={allProducts} fetchData={fetchData} />
                :
                <UserView productsData={allProducts} />

            }
        </>
    )
}
