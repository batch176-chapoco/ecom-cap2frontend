import { Form, Button, Container } from "react-bootstrap";
import { useEffect, useState, useContext } from 'react';
import UserContext from "../UserContext";
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';



export default function Register() {

    const { user, setUser } = useContext(UserContext);

    const navigate = useNavigate();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [verpassword, setVerpassword] = useState('')
        ;
    const [passwordStatus, setPasswordStatus] = useState('');


    //button
    const [isActive, setIsactive] = useState(true);

    useEffect(() => {
        if (email !== '' && password !== '' && verpassword !== '') {
            if (password !== verpassword) {
                setPasswordStatus("Password does not match!");
                setIsactive(false);
            } else {
                setPasswordStatus("Password match!");
                setIsactive(true);
            }
        } else {
            setIsactive(false);
        }
    }, [email, password, verpassword, passwordStatus])

    function registerUser(e) {
        e.preventDefault();

        fetch('https://capstone2trial.herokuapp.com/users/register', {
            method: 'POST',
            headers:
                { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data) {

                    if (data.message === `Email already exists.`) {
                        Swal.fire({
                            title: 'error',
                            icon: 'error',
                            text: 'Email already exists.'
                        })
                    } else {
                        Swal.fire({
                            title: 'Registration Successful!',
                            icon: 'success'
                        })
                        navigate('/login')
                    }
                }
                else {
                    Swal.fire({
                        title: 'error',
                        icon: 'error',
                        text: 'Please try again'
                    })
                }
            })
    }
    return (
        <Container>
            <Form onSubmit={e => registerUser(e)}>
                <h1>Register</h1>
                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter Email"
                        required
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter your Password"
                        required
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Re-enter your Password"
                        required
                        value={verpassword}
                        onChange={e => setVerpassword(e.target.value)} />
                </Form.Group>
                {passwordStatus === "Password match!"
                    ?
                    <p className="text-success">{passwordStatus}</p>
                    :
                    <p className="text-danger">{passwordStatus}</p>
                }

                {isActive ?
                    <Button variant="primary" type="submit"
                        className="mt-3">Submit</Button>
                    :
                    <Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>

                }
            </Form>
        </Container>
    )

}