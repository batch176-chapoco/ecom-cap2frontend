import { Form, Button } from "react-bootstrap";
import { Container } from "react-bootstrap";
import { Navigate, useNavigate } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from "../UserContext";
import Swal from "sweetalert2";


export default function Login() {

    const navigate = useNavigate();


    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    //button
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {
        if (email !== '' && password !== '') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password])

    function authentication(e) {
        e.preventDefault();

        fetch('https://capstone2trial.herokuapp.com/users/login',
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })

            .then(response => response.json())
            .then(data => {
                console.log(data)

                if (data.accessToken !== undefined) {
                    localStorage.setItem('accessToken', data.accessToken);
                    setUser({
                        accessToken: data.accessToken
                    })

                    Swal.fire({
                        title: 'Yay',
                        icon: 'success',
                        text: 'You are now Logged in!'
                    })

                    //get users details
                    fetch('https://capstone2trial.herokuapp.com/users/details', {
                        headers: {
                            Authorization: `Bearer ${data.accessToken}`
                        }
                    })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data)

                            if (data.isAdmin === true) {
                                localStorage.setItem('isAdmin', data.isAdmin)

                                setUser({
                                    isAdmin: data.isAdmin
                                })

                                navigate('/products')
                            } else {
                                navigate('/')
                            }
                        })

                } else {
                    Swal.fire({
                        title: 'Oooopssss',
                        icon: 'error',
                        text: 'Something went wrong. Check your Credentials'
                    })
                }

                setEmail('')
                setPassword('')
            })
    }



    return (

        // (user.accessToken !== null) ?

        //     <Navigate to="/products" />

        //     :

        <Container>
            <Form onSubmit={e => authentication(e)}>
                <h1>Login</h1>
                <Form.Group>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        required
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter your Password"
                        required
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                    />
                </Form.Group>

                {isActive ?
                    <Button variant="primary" type="submit">Submit</Button>
                    :
                    <Button variant="primary" type="submit" disabled>Submit</Button>

                }
            </Form>
        </Container>
    )
}