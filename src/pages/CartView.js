import { useState, useEffect } from 'react';
import { Form, Table, Button, TextField } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function CartView() {

    const [cart, setCart] = useState([]);
    const [quantity, setQuantity] = useState('');
    const [name, setName] = useState('');
    const [grandTotal, setGrandTotal] = useState(0);
    const [storedData, setStoredData] = useState(JSON.parse(localStorage.getItem('cartitems')));
    const navigate = useNavigate();

    useEffect(() => {

        //[incrementQuantity Fucntion]
        function incrementQuantity(prodId) {
            let updatedCartArray = [];
            for (let i = 0; i < storedData.length; i++) {
                if (storedData[i].productId === prodId) {
                    storedData[i].quantity += 1;
                    storedData[i].subTotal = storedData[i].quantity * storedData[i].price
                }
                updatedCartArray.push(storedData[i])

            }
            localStorage.setItem('cartitems', JSON.stringify(updatedCartArray));
            window.location.reload(false);
        }
        //[decrementQuantity Fucntion]
        function decrementQuantity(prodId) {
            let updatedCartArray = [];
            for (let i = 0; i < storedData.length; i++) {
                if (storedData[i].productId === prodId) {
                    if (storedData[i].quantity === 1) {
                        storedData[i].quantity = 1
                    } else {
                        storedData[i].quantity -= 1;
                        storedData[i].subTotal = storedData[i].quantity * storedData[i].price
                    }
                }
                updatedCartArray.push(storedData[i])
            }
            localStorage.setItem('cartitems', JSON.stringify(updatedCartArray));
            window.location.reload(false);
        }
        //[removeAnItem Function]
        function removeItem(prodId) {
            let updatedCartArray = [];
            for (let i = 0; i < storedData.length; i++) {
                if (storedData[i].productId === prodId) {
                    continue;
                }
                updatedCartArray.push(storedData[i])
            }
            localStorage.setItem('cartitems', JSON.stringify(updatedCartArray));
            window.location.reload(false);
        }

        if (localStorage.getItem('cartitems') == null) {
            localStorage.setItem('cartitems', '[]')
            Swal.fire({
                title: 'Cart Currently Empty',
                icon: 'question',
                confirmButtonColor: "#b36b14",
            })

            navigate('/products')
        } else {
            const cartArr = storedData.map(cartitem => {
                setGrandTotal(prevGrandTotal => prevGrandTotal + cartitem.subTotal)
                return (
                    <tr key={cartitem.productId}>
                        <td colSpan="8" className="bg-light text-success" style={{ fontWeight: 'bold' }}> {cartitem.name}
                        </td>
                        <td className="bg-light text-dark" style={{ fontWeight: 'bold' }}><span>&#8369;</span> {cartitem.price}
                        </td>
                        <td>
                            <Button className="mx-2" variant="dark" onClick={() => decrementQuantity(cartitem.productId)}> - </Button>
                            <span> {` ${cartitem.quantity} `} </span>
                            <Button className="mx-2" variant="dark" onClick={() => incrementQuantity(cartitem.productId)}> + </Button>

                        </td>
                        <td className="bg-light text-danger" style={{ fontWeight: 'bold' }} ><span>&#8369;</span>  {cartitem.subTotal}
                        </td>
                        <td>
                            <Button className="mx-2" variant="warning" onClick={() => removeItem(cartitem.productId)}> Remove </Button>
                        </td>

                    </tr>
                )
            })
            setStoredData(JSON.parse(localStorage.getItem('cartitems')))
            setCart(cartArr)
        }
    }, [quantity])

    //[ADD TO ORDER FUNCTION]

    const checkoutf = () => {
        Swal.fire({
            title: 'Success',
            icon: 'success',
        })
    }
    const addToOrders = () => {
        if (storedData.length === 0) {
            Swal.fire({
                title: 'Nothing Here yet. Wanna add a coffee?',
                icon: 'question',
                confirmButtonColor: "#b36b14",
            })
        } else {

            let newOrder = [];

            for (let i = 0; i < storedData.length; i++) {
                let cartItem = {
                    productId: storedData[i].productId,
                    quantity: storedData[i].quantity
                }
                newOrder.push(cartItem)

            }
            fetch('https://capstone2trial.herokuapp.com/users/order', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
                body: JSON.stringify(newOrder)
            })
                .then(res => res.json())
                .then(data => {

                    if (data) {

                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: `Checked out Succesfully`,
                            confirmButtonColor: "#b36b14",
                            showConfirmButton: false,
                            timer: 3000
                        })
                        localStorage.removeItem('cartitems');
                        navigate('/orders')
                    } else {
                        Swal.fire({
                            title: 'error!',
                            icon: 'error',
                            confirmButtonColor: "#b36b14",
                            text: 'Something went wrong.'
                        })
                    }
                })

        }
    }


    return (
        <>
            <div className="my-4" >
                <h1> {"Cart"} </h1>
            </div>

            <Table striped bordered hover responsive>
                <thead className="bg-info">
                    <tr>
                        <th colSpan="8">{"Name"}</th>
                        <th>{"Price"}</th>
                        <th>{"Quantity"}</th>
                        <th>{"Subtotal"}</th>
                        <th>{` `}</th>
                    </tr>
                </thead>

                <tbody>
                    {cart}
                </tbody>
            </Table>
            <Button className="p-3" variant="success" style={{ float: "right", fontWeight: 'bold' }} as={Link} to="/" onClick={checkoutf}> CHECKOUT </Button>
            <div className="bg-info mx-2 p-2"> <h3>{'TOTAL'} <span>&#8369;</span > {grandTotal} </h3>  </div>
        </>

    )
}
