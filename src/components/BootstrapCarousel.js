import React from 'react';
import { Carousel } from 'react-bootstrap';


export default function BootstrapCarousel() {
    return (
        <Carousel>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://cdn.shopify.com/s/files/1/0102/2974/3680/files/Petshop_Coverphoto_55d74306-45d4-476b-a4c8-800e51c64024_1200x.jpg?v=1631859168"
                    style={{ height: 700, width: 1400 }}
                    alt="First slide"
                />
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://media-exp1.licdn.com/dms/image/C4E1BAQHBVr3Kwo3X9Q/company-background_10000/0/1595862607895?e=2147483647&v=beta&t=02qs0d9gEFpfaVoEsgetC49w-vSkv1oYDce2OpVY2cg"
                    style={{ height: 700, width: 1400 }}
                    alt="Second slide"
                />
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://media.istockphoto.com/photos/food-and-accessories-for-the-dog-and-chihuahua-on-table-picture-id1341957270?b=1&k=20&m=1341957270&s=170667a&w=0&h=jNpI_9fB4RcBjPZ7Xj2AO6p1KO7bAhqudn3FQ85tvFM="
                    style={{ height: 700, width: 1400 }}
                    alt="Third slide"
                />
            </Carousel.Item>
        </Carousel>
    )
}