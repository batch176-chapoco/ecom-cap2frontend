import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { useContext } from 'react';


export default function AppNavBar() {


    const { user } = useContext(UserContext);
    return (
        <Navbar bg="info" expand="lg" variant="dark" className="mb-5">
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ms-auto">
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    <Nav.Link as={Link} to="/products">Products</Nav.Link>


                    {(user.accessToken !== null)

                        ?
                        <>
                            <Nav.Link as={Link} to="/cartview">Cart</Nav.Link>
                            <Nav.Link as={Link} to="/orders">Orders</Nav.Link>
                            <Nav.Link as={Link} to="/logout">Logout</Nav.Link>


                        </>
                        :
                        <>
                            <Nav.Link as={Link} to="/login">Login</Nav.Link>
                            <Nav.Link as={Link} to="/register">Register</Nav.Link>

                        </>

                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

