import React from 'react';
import { Carousel } from 'react-bootstrap';


export default function CarouselText() {
    return (
        <Carousel fade>
            <Carousel.Item>
                <Carousel.Caption>
                    <h3 className="text-dark">First slide label</h3>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <Carousel.Caption>
                    <h3>Second slide label</h3>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <Carousel.Caption>
                    <h3>Third slide label</h3>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    )
}