import { Button } from "react-bootstrap";
import Swal from 'sweetalert2';



export default function ArchiveProduct({ product, isActive, fetchData }) {



    const archiveToggle = (productId) => {
        console.log(productId)
        fetch(`https://capstone2trial.herokuapp.com/products/${productId}/archive`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    Swal.fire({
                        title: 'success',
                        icon: 'success',
                        text: 'Product status changed to Inactive'
                    })
                    fetchData()
                } else {
                    Swal.fire({
                        title: 'error',
                        icon: 'error',
                        text: 'Something went wrong'
                    })
                    fetchData()
                }
            })
    }

    const activateToggle = (productId) => {
        fetch(`https://capstone2trial.herokuapp.com/products/${productId}/activate`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    Swal.fire({
                        title: 'success',
                        icon: 'success',
                        text: 'Product successfully activated'
                    })
                    fetchData()
                } else {
                    Swal.fire({
                        title: 'error',
                        icon: 'error',
                        text: 'Something went wrong'
                    })
                    fetchData();
                }
            })
    }
    return (
        <>
            {isActive ?


                <Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Disable</Button>
                :
                <Button variant="success" size="sm" onClick={() => activateToggle(product)}>Enable</Button>

            }




        </>
    )
}


